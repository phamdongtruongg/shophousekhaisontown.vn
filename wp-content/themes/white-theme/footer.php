<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package white_theme
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
	
</div><!-- #page -->
<script>
    $(document).ready(function () {
        $('#lh-fixed').click(function () {
            const form = $("#form-lh-main").offset().top;
            $("html, body").animate({
                scrollTop: form
            }, 600);
            return false;
        });
    });
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<?php wp_footer(); ?>

</body>
</html>
